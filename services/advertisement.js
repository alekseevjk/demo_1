const AdvertisementModel = require('../Models/Advertisement');

class AdvertisementService {
    static async getAll() {
        return AdvertisementModel.find();
    }
    static async create(title, description) {

        const Adv = new AdvertisementModel({ title, description });

        return Adv.save()
    }


    static async update(id, update) {
        return AdvertisementModel.findOneAndUpdate({_id:id}, update, { new: true });
    }

    static async delete(id) {
        return AdvertisementModel.deleteOne({_id:id});
    }

    static async get(id) {
        return AdvertisementModel.findById(id);
    }

}

module.exports = AdvertisementService;