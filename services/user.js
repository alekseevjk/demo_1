const UserModel = require('../Models/User');

class UserService {
    static async getAll() {
        return UserModel.find();
    }
    static async create(name, lastname, password,email, phone) {
        const user = new UserModel({ name, lastname, password,email, phone });
        return user.save();
    }


    static async update(id, update) {
        return UserModel.findOneAndUpdate({_id:id}, update, { new: true });
    }

    static async delete(id) {
        return UserModel.deleteOne({_id:id});
    }

    static async get(id) {
        return UserModel.findById(id);
    }

}

module.exports = UserService;