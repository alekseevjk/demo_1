const UserService = require('../services/user');


class  UserController{
    static async getAll(req,res,next){
        try{
            const user = await UserService.getAll()

            return res.json({ ok: true, result: user });
        }catch(err){
            next(err)
        }
    }

    static async get(req,res,next){
        try{
            const { id } = req.params
            const user = await UserService.get(id)
            return res.json({ ok: true, result: user });
        }catch(err){
            next(err)
        }
    }

    static async create(req,res,next){
        try{
            const { name, lastname, password,email, phone } = req.body
            const user = await UserService.create( name, lastname, password,email, phone )
            return res.json({ ok: true, result: user });
        }catch(err){
            next(err)
        }
    }


    static async update(req,res,next){
        try{
            const {id} = req.params
            const user = await UserService.update(id,req.body)
            return res.json({ ok: true, result: user });
        }catch(err){
            next(err)
        }
    }
    static async delete(req,res,next){
        try{
            const {id} = req.params
            const user = await UserService.delete(id)
            return res.json({ ok: true, result: user });
        }catch(err){
            next(err)
        }
    }


}


module.exports = UserController;