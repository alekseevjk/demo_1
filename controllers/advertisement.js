const AdvertisementService = require('../services/advertisement');

class  AdvertisementController{

    static async getAll(req,res,next){
        try{
            const advertisements = await AdvertisementService.getAll()

            return res.json({ ok: true, result: advertisements });
        }catch(err){
            next(err)
        }
    }

    static async get(req,res,next){
        try{
            const {id} = req.params
            const advertisement = await AdvertisementService.get(id)
            return res.json({ ok: true, result: advertisement });
        }catch(err){
            next(err)
        }
    }

    static async create(req,res,next){

        try{

            const { title,description } = req.body
            console.log('check_controller_ 1',title,description,req.body)
            const advertisement = await AdvertisementService.create(title,description)
            console.log('check_controller')
            return res.json({ ok: true, result: advertisement });
        }catch(err){
            next(err)
        }
    }


    static async update(req,res,next){
        try{
            const {id} = req.params
            const advertisement = await AdvertisementService.update(id,req.body)
            return res.json({ ok: true, result: advertisement });
        }catch(err){
            next(err)
        }
    }
    static async delete(req,res,next){
        try{
            const {id} = req.params
            console.log('delete_1')
            const advertisement = await AdvertisementService.delete(id)

            return res.json({ ok: true, result: advertisement });
        }catch(err){
            next(err)
        }
    }


}

module.exports = AdvertisementController;