const express = require('express')
const mongoose = require('mongoose')
const path = require('path')
const { DB_URI, IP, PORT } = require('./config')
const errorHandler = require('./middlewares/error')
// const loggerRequest = require('./middlewares/logger.req')
const logger_req = require('./logger')
const app = express()

const advertisementRoutes=require('./routes/advertisement')
const userRoutes=require('./routes/user')


//config

app.use(express.static('public'))
app.use(express.urlencoded({extended:true}))

app.use(express.json())

app.use((req, res, next) => {return logger_req.info({req, res, next})})
//routes
app.get('/',(req,res,next) => {
    res.send("Home page")
})

app.use('/api/user',userRoutes)
app.use('/api/advertisement',advertisementRoutes)




app.use(errorHandler);




async function start(){
    try{
        await mongoose.connect(DB_URI,{useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true })
        app.listen(PORT,()=>{
            console.log(`application is running, ${IP}:${PORT}`)
        })
    }catch(e){
        console.log(e)
    }
}
start()

