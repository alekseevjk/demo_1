const {createLogger, transports, format} = require('winston');
function logger() {
    return  createLogger({
            level: 'info',
            transports: [new transports.Console(),new transports.File({ filename: 'combined.log' })],
            format: format.combine(
                format.timestamp(),
                format.printf(info => {
                    const {req, res, next} = info.message;
                    const ip = (req.headers['x-forwarded-for'] || '').split(',').pop() ||
                        req.connection.remoteAddress ||
                        req.socket.remoteAddress ||
                        req.connection.socket.remoteAddress
                    next()
                    const data = [{date:info.timestamp,headers: req.headers, body:req.body, route: req.route,ip:ip}]
                    return JSON.stringify(data)
                })
            )
        })

}

module.exports = logger()