const {Router} = require('express')
const router = Router()

const AdvertisementController = require('../controllers/advertisement')
const {advertisementValedators,CheckErrorsValid} = require('../middlewares/express.validator')


// Получение всех объявлений
router.get('/',[],AdvertisementController.getAll)
// Получение объявления
router.get('/:id',[],AdvertisementController.get)


// Добавление нового объявления
router.post('/',[advertisementValedators,CheckErrorsValid],AdvertisementController.create)

// Редактирование объявления
router.put('/:id',[advertisementValedators,CheckErrorsValid],AdvertisementController.update)

// Удаление объявления
router.delete('/:id',[],AdvertisementController.delete)

module.exports = router