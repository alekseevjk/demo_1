const {Router} = require('express')
const router = Router()
const {userValidators,CheckErrorsValid} = require('../middlewares/express.validator')
const UserController = require('../controllers/user')

// Получение всех пользователей
router.get('/',[],UserController.getAll )

// Получение пользователя
router.get('/:id',[],UserController.get )



// Добавление нового пользователя
router.post('/',[userValidators,CheckErrorsValid],UserController.create)

// Редактирование пользвоателя
router.put('/:id',[userValidators,CheckErrorsValid],UserController.update)

// Удаление пользователя
router.delete('/:id',[],UserController.delete)


module.exports = router