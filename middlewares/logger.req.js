function getDateTime(date) {
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    return year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;
}

const fs = require('fs')
const path = require('path')
const log_filename = `log ${getDateTime(new Date()).toString()}.txt`

module.exports = (req,res,next) => {
    console.log('middleWare log')
    const async_fs = async ()=>{
         const a = await fs.mkdir(path.join(__dirname,'./..','logs'),err => {
            //if (err) throw err
            console.log('Папка создана')
        })

        const ip = (req.headers['x-forwarded-for'] || '').split(',').pop() ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress

        const b = await fs.appendFile(path.join(__dirname,'./..','logs','log_filename.txt'),
            "\n"+getDateTime(new Date())+ "\nHEADERS:" +JSON.stringify(req.headers)+"\nBODY: "+ JSON.stringify(req.body)
            + "\nROUTE: " + JSON.stringify(req.route) +"\nIP: "+ ip,
            (err) => {
                if (err) throw err

            })
    }
    async_fs()
    next()
}