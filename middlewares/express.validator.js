const {body,validationResult} = require('express-validator')
exports.userValidators = [
    body('name').isString().trim().isLength({min: 3,max: 56}).withMessage('Имя должно быть минимум 3 символа'),
    body('lastname').isString().trim().isLength({min: 3,max: 56}).withMessage('Имя должно быть минимум 3 символа'),
    body('password','Пароль должен быть минимум 6 символов')
        .isString().trim().isLength({min: 6,max: 56}).isAlphanumeric().withMessage('Можно использовать только цифры и буквы'),
    body('email').isString().trim().isEmail().withMessage('Введите корректный email'),
    body('phone').isString().trim().isLength({min: 4,max: 14}).withMessage('Неверный формат номера телефона')

]
exports.advertisementValedators = [
    body('title').isString().trim(),
    body('description').isString().trim()

]
exports.CheckErrorsValid = [(req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    next()
}]
