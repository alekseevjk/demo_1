const { Schema, model } = require('mongoose');

const userSchema = new Schema({

    name: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: String,
        required: false
    },
    favorites: [{ // Нужна сессия, чтобы привзяать к авторизованному создание
        AdvertisementId: {
            type: Schema.Types.ObjectId,
            ref: 'Advertisment',
            required: true
        }
    }]



}, { autoIndex: true, timestamps: true });

module.exports = model('User', userSchema);