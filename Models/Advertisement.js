const { Schema, model } = require('mongoose');

const advertismentSchema = new Schema({


        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        }


});

module.exports = model('Advertisment', advertismentSchema);